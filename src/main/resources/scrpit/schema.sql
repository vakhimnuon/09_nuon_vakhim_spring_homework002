
CREATE DATABASE Mybatis;

CREATE TABLE Product (
                         product_id serial primary key,
                         product_name varchar(100) not null ,
                         product_price decimal not null
);
CREATE TABLE Customer(
                         customer_id serial NOT NULL PRIMARY KEY ,
                         customer_name varchar(100) NOT NULL ,
                         customer_address varchar(100) NOT NULL ,
                         customer_phone varchar(100) NOT NULL
);
CREATE TABLE Invoice (
                         invoice_id serial NOT NULL PRIMARY KEY ,
                         invoice_date date default now(),
                         customer_id INT NOT NULL ,
                         FOREIGN KEY (customer_id) REFERENCES Customer (customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE Invoice_detail (
                                id serial primary key NOT NULL ,
                                invoice_id INT NOT NULL ,
                                product_id INT NOT NULL ,
                                FOREIGN KEY (invoice_id) REFERENCES Invoice(invoice_id) ON DELETE CASCADE,
                                FOREIGN KEY (product_id) REFERENCES product(product_id) ON DELETE CASCADE
);