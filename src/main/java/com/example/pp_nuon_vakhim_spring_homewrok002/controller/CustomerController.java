package com.example.pp_nuon_vakhim_spring_homewrok002.controller;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Customer;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.CustomerRequest;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.response.ApiRespone;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("api/v1/customer/show")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new ApiRespone<List<Customer>>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read customer successfully",
                customerService.getAllCustomer()
        ));
    }
    @GetMapping("api/v1/customer/read/{id}")
    public ResponseEntity<?> readCustomerById(@PathVariable Integer id){
        Customer customer = customerService.readCustomerById(id);
        ApiRespone<Customer> respone = new ApiRespone<>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read customer successfully",
                customer);
        return ResponseEntity.ok(respone);
    }
    @PostMapping("api/v1/customer/insert")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(new ApiRespone<Customer>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert customer Successfully",
                customerService.insertCustomer(customer)
        ));
    }
    @PutMapping("api/v1/customer/update/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest){
      customerService.updateCustomerById(id, customerRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update customer Successfully",
                null
        ));
    }
    @DeleteMapping("api/v1/customer/delete/{id}")
    public ResponseEntity<?>deleteCustomer(@PathVariable Integer id) {
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Delete customer Successfully",
                null
        ));
    }

}
