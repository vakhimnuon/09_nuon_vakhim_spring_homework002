package com.example.pp_nuon_vakhim_spring_homewrok002.controller;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Product;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.ProductRequest;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.response.ApiRespone;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("api/v1/product/show")
    public List<Product> getAllProduct(){
          return productService.getAllProdcut();
    }
    @GetMapping("api/v1/product/read/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id){
        Product product = productService.getProdcutById(id);
        ApiRespone<Product> respone = new ApiRespone<>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read product successfully",
                product
        );
        return ResponseEntity.ok(respone);
    }
    @PostMapping("api/v1/product/insert")
    public ResponseEntity<?> insertProduct(@RequestBody Product product){
        return ResponseEntity.ok(new ApiRespone<Product>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Insert product Successfully",
                productService.insertProduct(product)
                ));
    }
    @PutMapping("api/v1/product/update/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer id, @RequestBody ProductRequest productRequest){
        productService.updateProductById(id, productRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Update product Successfully",
                null
        ));
    }
    @DeleteMapping("api/v1/product/delete/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer id){
        productService.deleteProductById(id);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Delete product Successfully",
                null
        ));
    }
}
