package com.example.pp_nuon_vakhim_spring_homewrok002.controller;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.response.ApiRespone;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.InvoicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class InvoiceContoller {
    private final InvoicService invoicService;

    public InvoiceContoller(InvoicService invoicService) {
        this.invoicService = invoicService;
    }
    @GetMapping("api/v1/invoice/show")
    public ResponseEntity<?> getAllInvoice(){
        return ResponseEntity.ok(new ApiRespone<List<Integer>>(
                LocalDateTime.now(), HttpStatus.OK,
                "Read customer successfully",
                invoicService.getAllInvoice()
                ));
    }

}
