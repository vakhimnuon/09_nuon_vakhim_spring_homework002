package com.example.pp_nuon_vakhim_spring_homewrok002.repository;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Customer;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("""
             SELECT * FROM customer
             """)
    @Result(property ="customerId", column = "customer_id")
    @Result(property ="customerName", column = "customer_name")
    @Result(property ="customerAddress", column = "customer_address")
    @Result(property ="customerPhone", column = "customer_phone")
    List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer
             WHERE customer_id = #{id};
            """)
    @Result(property ="customerId", column = "customer_id")
    @Result(property ="customerName", column = "customer_name")
    @Result(property ="customerAddress", column = "customer_address")
    @Result(property ="customerPhone", column = "customer_phone")
    Customer readCustomerById(Integer id);

    @Select("""
            INSERT INTO customer (customer_name, customer_address, customer_phone)
            values (#{customer.customerName}, #{customer.customerAddress},#{customer.customerPhone})
            RETURNING *
            """)
    @Result(property ="customerId", column = "customer_id")
    @Result(property ="customerName", column = "customer_name")
    @Result(property ="customerAddress", column = "customer_address")
    @Result(property ="customerPhone", column = "customer_phone")
    Customer insertCustomer(@Param("customer") Customer customer);

    @Update("""
            UPDATE customer
            SET customer_name = #{customer.name}, customer_address=#{customer.address}, customer_phone=#{customer.phone}
            WHERE customer_id =#{customer_id};
            """)
    void updateCustomerById(@Param("customer_id") Integer id, @Param("customer") CustomerRequest customerRequest);

    @Delete("""
            DELETE FROM customer
            WHERE customer_id=#{id};
            """)
    void deleteCustomerById(Integer id);
}
