package com.example.pp_nuon_vakhim_spring_homewrok002.repository;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Product;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    //Show all product
    @Select("""
            SELECT * FROM product
            """)
    @Result(property ="productId", column = "product_id")
    @Result(property ="productName", column = "product_name")
    @Result(property ="productPrice", column = "product_price")
    List<Product>  getAllProduct();

    //Read product by ID
    @Select("""
             SELECT * FROM product 
             WHERE product_id = #{id};""")
    @Result(property ="productId", column = "product_id")
    @Result(property ="productName", column = "product_name")
    @Result(property ="productPrice", column = "product_price")
    Product getProductById(Integer id);

    //Insert product
    @Select("""
            INSERT INTO product ( product_name, product_price)
            values (#{product.productName}, #{product.productPrice})
            RETURNING *
            """)
    @Result(property ="productId", column = "product_id")
    @Result(property ="productName", column = "product_name")
    @Result(property ="productPrice", column = "product_price")
    Product insertProduct(@Param("product") Product product);

    @Update("""
            UPDATE product
            SET product_name = #{product.name}, product_price=#{product.price}
            WHERE product_id =#{product_id};
            """)
    void updateProductById(@Param("product_id") Integer id,@Param("product") ProductRequest productRequest);

    @Delete("""
            DELETE FROM product
            WHERE product_id=#{id};
            """)
    void deleteProductById(Integer id);

}
