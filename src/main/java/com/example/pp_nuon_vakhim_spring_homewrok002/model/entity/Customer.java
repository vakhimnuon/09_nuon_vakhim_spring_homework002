package com.example.pp_nuon_vakhim_spring_homewrok002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
