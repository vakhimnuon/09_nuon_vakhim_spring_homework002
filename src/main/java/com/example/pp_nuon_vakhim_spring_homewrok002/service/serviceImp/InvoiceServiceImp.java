package com.example.pp_nuon_vakhim_spring_homewrok002.service.serviceImp;

import com.example.pp_nuon_vakhim_spring_homewrok002.repository.InvoiceRepository;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.InvoicService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoicService {
    private InvoiceRepository invoiceRepository;

    @Override
    public List<Integer> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }
}
