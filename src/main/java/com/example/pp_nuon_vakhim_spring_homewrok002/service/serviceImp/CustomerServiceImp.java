package com.example.pp_nuon_vakhim_spring_homewrok002.service.serviceImp;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Customer;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.CustomerRequest;
import com.example.pp_nuon_vakhim_spring_homewrok002.repository.CustomerRepository;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;
    @Autowired
    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer readCustomerById(Integer id) {
        return customerRepository.readCustomerById(id);
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public Customer updateCustomerById(Integer id, CustomerRequest customerRequest) {
        customerRepository.updateCustomerById(id,customerRequest);
        return null;
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepository.deleteCustomerById(id);
    }
}
