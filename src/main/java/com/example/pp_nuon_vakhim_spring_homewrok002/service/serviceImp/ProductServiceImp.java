package com.example.pp_nuon_vakhim_spring_homewrok002.service.serviceImp;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Product;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.ProductRequest;
import com.example.pp_nuon_vakhim_spring_homewrok002.repository.ProductRepository;
import com.example.pp_nuon_vakhim_spring_homewrok002.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
   private ProductRepository productRepository;

   @Autowired

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProdcut() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProdcutById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product insertProduct(Product product) {
        return productRepository.insertProduct(product);
    }

    @Override
    public Product updateProductById(Integer id, ProductRequest productRequest) {
        productRepository.updateProductById(id, productRequest);
        return null;
    }

    @Override
    public void deleteProductById(Integer id) {
        productRepository.deleteProductById(id);
    }
}
