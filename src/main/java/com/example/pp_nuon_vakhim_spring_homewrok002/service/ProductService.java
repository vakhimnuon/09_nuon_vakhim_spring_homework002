package com.example.pp_nuon_vakhim_spring_homewrok002.service;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Product;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProdcut();

    Product getProdcutById(Integer id);

    Product insertProduct(Product product);

    Product updateProductById(Integer id, ProductRequest productRequest);

    void deleteProductById(Integer id);


//    public String sendSmg();
};
