package com.example.pp_nuon_vakhim_spring_homewrok002.service;

import com.example.pp_nuon_vakhim_spring_homewrok002.model.entity.Customer;
import com.example.pp_nuon_vakhim_spring_homewrok002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer readCustomerById(Integer id);

    Customer insertCustomer(Customer customer);

    Customer updateCustomerById(Integer id, CustomerRequest customerRequest);

    void deleteCustomerById(Integer id);
}
